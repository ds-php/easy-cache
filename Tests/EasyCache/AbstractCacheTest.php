<?php

namespace Tests\EasyCache;

use Ds\EasyCache\Cache;
use Ds\EasyCache\CacheStorageInterface;
use Ds\EasyCache\Storage\NullStorage;
use Tests\EasyCache\Mock\AbstractCacheMock;

/**
 * Abstract Cache Tests
 *
 * @package Tests\Cache
 */
class AbstractCacheTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Cache
     */
    public $cache;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $storageMock;

    /**
     * Using AbstractCacheMock to extend abstract class.
     */
    public function setUp() : void
    {
        $this->storageMock = $this->getMockBuilder(CacheStorageInterface::class)->getMock();
        $this->cache = new Cache($this->storageMock);
    }

    /**
     * Test that NullStorage Adaptor is called by default.
     */
    public function testConstructNullStorage(){
        $cache = new Cache();
        $storage = $cache->getCacheStorage();
        $this->assertInstanceOf(NullStorage::class,$storage);
    }

    /**
     * Test CacheStorage is updated from construct.
     */
    public function testConstructWithStorage(){
        $cache = new Cache($this->storageMock);
        $storage = $cache->getCacheStorage();
        $this->assertSame($this->storageMock,$storage);
    }

    /**
     * Test that Cache::withCacheStorage() updates CacheStorageInterface adaptor.
     */
    public function testWithCacheStorage()
    {
        $newStorageMock = clone $this->storageMock;
        $newCache = $this->cache->withCacheStorage($newStorageMock);
        $this->assertEquals($newStorageMock, $newCache->getCacheStorage());
    }

    /**
     * Test that CacheStorageInterface is returned from Cache::getCacheStorage
     */
    public function testGetCacheStorage()
    {
        $expected = $this->storageMock;
        $actual = $this->cache->getCacheStorage();
        $this->assertEquals($expected, $actual);
    }
}
