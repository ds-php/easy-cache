<?php

namespace Tests\EasyCache\Storage;

use Ds\EasyCache\Storage\NullStorage;

/**
 * Class NullStorageTest
 *
 * @package Tests\Cache\Storage
 */
class NullStorageTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var NullStorage
     */
    public $cacheStorage;

    /**
     *
     */
    public function setUp() : void
    {
        $this->cacheStorage = new NullStorage();
    }

    /**
     *
     */
    public function testHas(){
        $this->assertEquals($this->cacheStorage->has('key', 'value'), false);
    }

    /**
     *
     */
    public function testSet(){
        $this->assertEquals($this->cacheStorage->set('key', 'value', 10), true);
    }

    /**
     *
     */
    public function testGet(){
        $this->assertEquals($this->cacheStorage->get('key'), null);
    }

    /**
     *
     */
    public function testDelete(){
        $this->assertEquals($this->cacheStorage->delete('key'), true);
    }

    /**
     *
     */
    public function testClear(){
        $this->assertEquals($this->cacheStorage->clear(), true);
    }
    
}
