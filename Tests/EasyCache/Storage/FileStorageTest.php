<?php

namespace Tests\EasyCache\Storage;

use Ds\EasyCache\CacheException;
use Ds\EasyCache\InvalidArgumentException;
use Ds\EasyCache\Storage\FileStorage;
use Ds\EasyCache\DateTime\TimeConversion;

/**
 * Class FileStorageTest
 *
 * @package Tests\Cache\Storage
 */
class FileStorageTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var FileStorage
     */
    public $cacheStorage;

    /**
     * @var
     */
    public $testDir;

    /**
     *
     */
    public function setUp() : void
    {
        $this->testDir = __DIR__ . '/cacheTest';
        $this->cacheStorage = new FileStorage( $this->testDir, new \DateInterval('P1M'));
    }

    /**
     * Clean up any cache files left on system.
     */
    public function tearDown() : void
    {
        $dir = $this->testDir;

        if (is_dir( $dir)){
            array_map('unlink', glob("$dir/*.*"));
        }
        \rmdir($dir);
    }

    /**
     * Test that InvalidArgumentException is thrown when not using a valid string
     */
    public function testNonStringDirectory()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->cacheStorage = new FileStorage(false, new \DateInterval('P1M'));
    }

    /**
     * Test that CacheException is thrown if directory is not valid/writable.
     */
    public function testUnwritableDirectory(){

        $this->expectException(CacheException::class);
        $this->cacheStorage = new FileStorage('php://memory', new \DateInterval('P1M'));
    }

    /**
     * Test that set is called.
     */
    public function testSet(){
        $actual = $this->cacheStorage->set('key','value',60*60);
        $this->assertEquals(true, $actual);
    }

    /**
     * Test has when not value is found.
     */
    public function testHasNoValue(){
        $this->assertEquals($this->cacheStorage->has('someRandomKey'), false);
    }

    /**
     * Test has when value is found.
     */
    public function testHas(){
        $this->cacheStorage->set('foo','bar');
        $this->assertEquals($this->cacheStorage->has('foo'), true);
    }

    /**
     * Test has when value is found but has expired.
     */
    public function testHasExpired(){
        $this->cacheStorage->set('expired','bar', -1200);
        $this->assertEquals($this->cacheStorage->has('expired'), false);
    }

    /**
     * Test that clear is called.
     */
    public function testClear(){
       $this->assertEquals($this->cacheStorage->clear(), true);
    }

    /**
     * Test that get returns a found key.
     */
    public function testGet(){
        $expected = 'bat';
        $this->cacheStorage->set('baz',$expected);
        $actual = $this->cacheStorage->get('baz');
        $this->assertEquals($expected, $actual);
    }

    public function testDelete(){
        $this->cacheStorage->delete('baz');
        $this->assertEquals($this->cacheStorage->has('baz'), false);
    }

    public function testCreateCacheItem(){

        $reflector = new \ReflectionClass(FileStorage::class);
        $method = $reflector->getMethod('_createCacheItem');
        $method->setAccessible(true);

        $key= 'my-key_+';
        $value = 'my-value';
        $ttl = 60 * 60 * 7;

        $expected = [
            'file' => $this->testDir . DIRECTORY_SEPARATOR . implode('.',[$key, FileStorage::EXTENSION]),
            'data' => json_encode([$ttl, $value])
        ];

        $actual = $method->invokeArgs($this->cacheStorage, [$key, $value, $ttl]);
        $this->assertEquals($expected, $actual);
    }

    public function testCreateCacheItemInvalidKey(){

        $this->expectException(InvalidArgumentException::class);
        $reflector = new \ReflectionClass(FileStorage::class);
        $method = $reflector->getMethod('_createCacheItem');
        $method->setAccessible(true);
        $key = '\jlasd$dll.';
        $value = 'my-value';
        $ttl = 60 * 60 * 7;
        $method->invokeArgs($this->cacheStorage, [$key, $value, $ttl]);
    }

    public function testFetchCacheNoItem(){
        $reflector = new \ReflectionClass(FileStorage::class);
        $method = $reflector->getMethod('_fetchCacheFile');
        $method->setAccessible(true);
        $key= 'unknown';
        $actual = $method->invokeArgs($this->cacheStorage, [$key]);
        $this->assertEquals(false, $actual);
    }

    public function testFetchCacheValid(){

        $key= 'my-item';
        $ttl = new \DateInterval('PT1M');
        $value = 'some-value';

        $this->cacheStorage->set($key, $value, $ttl);

        $dateTime = new \DateTime();
        $dtnow = $dateTime->getTimestamp();

        $dateTime->add( $ttl );
        $ttl = $dateTime->getTimestamp();

        $expected = [
            0 => $ttl,
            1 => $value
        ];

        $reflector = new \ReflectionClass(FileStorage::class);
        $method = $reflector->getMethod('_fetchCacheFile');
        $method->setAccessible(true);
        $actual = $method->invokeArgs($this->cacheStorage, [$key]);
       
        $this->assertEquals($expected, $actual);
    }

    public function testFetchCacheExpired(){
        $key= 'my-item';
        $ttl = -1500;
        $value = 'some-value';
        $this->cacheStorage->set($key, $value, $ttl);
        $reflector = new \ReflectionClass(FileStorage::class);
        $method = $reflector->getMethod('_fetchCacheFile');
        $method->setAccessible(true);
        $actual = $method->invokeArgs($this->cacheStorage, [$key]);
        $this->assertEquals(false, $actual);
    }

    
}
