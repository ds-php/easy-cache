<?php

namespace Tests\EasyCache\Storage;

use Tests\EasyCache\Mock\AbstractStorageMock;

/**
 * Class MemcacheStorageTest
 * @package Tests\Cache
 */
class AbstractStorageTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var int
     */
    public $ttl;

    /**
     * @var AbstractStorageMock
     */
    public $abstractStorage;

    /**
     * AbstractStorage Setup.
     * AbstractStorageMock simply extends AbstractStorage.
     */
    public function setUp() : void
    {
        $this->ttl = 60 * 60 * 1;
        $this->abstractStorage = new AbstractStorageMock(new \DateInterval('PT1H'));
    }

    /**
     * Test immutable setter AbstractStorage::withTtl
     */
    public function testWithTtl(){
        $newDateInterval = new \DateInterval('PT3H');
        $newTtl = $this->abstractStorage->withTtl($newDateInterval);
        $this->assertNotSame($this->abstractStorage->getTtl(), $newTtl->getTtl());
    }

    /**
     * Test getter AbstractStorage::getTtl
     */
    public function testGetTtl(){
        $ttl = $this->abstractStorage->getTtl();
        $this->assertSame($this->ttl, $ttl);
    }

}
