<?php

namespace Tests\EasyCache\DateTime;

use Ds\EasyCache\DateTime\TimeConversion;

/**
 * Class MemcacheStorageTest
 * @package Tests\Cache
 */
class DateTimeTest extends \PHPUnit\Framework\TestCase
{
    public $ttl;

    public function setUp() : void
    {
        $this->ttl = 60 * 60 * 1;
    }

    /**
     * testGetTTLfromDateInterval
     */
    public function testGetTTLfromDateInterval(){
        $expires = new \DateInterval('PT1H');
        $expected = $this->ttl;
        $actual = TimeConversion::GetTTLfromDateInterval($expires);
        $this->assertEquals($expected, $actual);
    }

     /**
     * testGetTTLDefault
     */
    public function testGetTTLDefault(){
        $actual = TimeConversion::GetTTL($this->ttl, null);
        $this->assertEquals($this->ttl, $actual);
    }

    /**
     * testGetTTL
     */
    public function testGetTTL(){
        $expires = new \DateInterval('PT2H');
        $expected = 60 * 60 * 2;
        $actual = TimeConversion::GetTTL($this->ttl, $expires);
        $this->assertEquals($expected, $actual);
    }

    /**
     * testGetTTLInt
     */
    public function testGetTTLInt(){
        $expires = 3600;
        $expected = $expires;
        $actual = TimeConversion::GetTTL($this->ttl, $expires);
        $this->assertEquals($expected, $actual);
    }

}
