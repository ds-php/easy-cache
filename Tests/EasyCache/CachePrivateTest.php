<?php

namespace Tests\EasyCache;

use Ds\EasyCache\Cache;
use Ds\EasyCache\CacheStorageInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Tests\EasyCache\Mock\IteratorMock;

/**
 * Protected/Private Method Tests for Ds\Cache\Cache
 *
 * @package Tests\Cache
 */
class CachePrivateTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Cache
     */
    public $cache;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $storageMock;

    public function setUp() : void
    {
        $this->storageMock = $this->getMockBuilder(CacheStorageInterface::class)->getMock();
        $this->cache = new Cache($this->storageMock);
    }

    /**
     * Test that array has keys.
     */
    public function testArrayHasKeys(){
        $data = ['a' => 'foo', 'b' => 'bar'];
        $method = $this->getCacheMethod('_hasKeys');
        $actual = $method->invokeArgs($this->cache, [$data]);
        $this->assertEquals(true, $actual);
    }

    /**
     * Test exception thrown when array is missing keys.
     */
    public function testArrayHasNoKeys(){
        $this->expectException(InvalidArgumentException::class);
        $data = ['foo','bar'];
        $method = $this->getCacheMethod('_hasKeys');
        $method->invokeArgs($this->cache, [$data]);
    }

    /**
     * Test array contains a failure.
     */
    public function testHasFailure(){
        $results = [true,true,true,false,true];
        $method = $this->getCacheMethod('_hasFailure');
        $actual = $method->invokeArgs($this->cache, [$results]);
        $expected = true;
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testHasNoFailure(){
        $results = [true,true,true,true,true];
        $method = $this->getCacheMethod('_hasFailure');
        $actual = $method->invokeArgs($this->cache, [$results]);
        $expected = false;
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that array is Traversable
     */
    public function testIsTraversable(){
        $data = ['a','b','c','d'];
        $method = $this->getCacheMethod('_isTraversable');
        $actual = $method->invokeArgs($this->cache, [$data]);
        $expected = true;
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that string is not Traversable
     */
    public function testIsNotTraversable(){
        $this->expectException(InvalidArgumentException::class);
        $data = 'some-random-string';
        $method = $this->getCacheMethod('_isTraversable');
        $method->invokeArgs($this->cache, [$data]);
    }

    /**
     * Test that instance of Iterator is traversable
     */
    public function testIsTraversableIterator(){
        $iterator = new IteratorMock();
        $method = $this->getCacheMethod('_isTraversable');
        $actual = $method->invokeArgs($this->cache, [$iterator]);
        $expected = true;
        $this->assertEquals($expected, $actual);
    }

    /**
     * Reflect protected/private Methods.
     * @param $method
     * @return \ReflectionMethod
     * @throws \ReflectionException
     */
    private function getCacheMethod($method){
        $reflector = new \ReflectionClass(Cache::class);
        $method = $reflector->getMethod($method);
        $method->setAccessible(true);
        return $method;
    }

}
