<?php

namespace Tests\EasyCache\Mock;

use Ds\EasyCache\Storage\AbstractStorage;

/**
 * Class AbstractStorageMock
 *
 * Mock class to test Abstract Methods.
 *
 * @package Tests\Cache\Mock
 */
class AbstractStorageMock extends AbstractStorage{

    public $ttl = 0;
    public function set($key, $value, $ttl = null){}
    public function has($key){}
    public function get($key){}
    public function delete($key){}
    public function clear(){}
}