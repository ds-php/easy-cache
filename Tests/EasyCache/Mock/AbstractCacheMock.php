<?php

namespace Tests\EasyCache\Mock;

use Ds\EasyCache\AbstractCache;

/**
 * Class AbstractCacheMock
 *
 * Mock class to test Abstract Methods.
 *
 * @package Tests\Cache\Mock
 */
abstract class AbstractCacheMock extends AbstractCache{
}