<?php

namespace Tests\EasyCache\Mock;

/**
 * Class IteratorMock
 *
 * Mock class to test Iterator instanceOf
 *
 * @package Tests\Cache\Mock
 */
class IteratorMock implements \Iterator{
    public function current(){}
    public function next(){}
    public function key(){}
    public function valid(){}
    public function rewind(){}
}
