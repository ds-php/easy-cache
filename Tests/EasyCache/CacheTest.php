<?php

namespace Tests\EasyCache;

use Ds\EasyCache\Cache;
use Ds\EasyCache\CacheStorageInterface;
use Ds\EasyCache\Storage\AbstractStorage;
use Psr\SimpleCache\InvalidArgumentException;
use Tests\EasyCache\Mock\AbstractStorageMock;

/**
 * Cache Tests
 *
 * @package Tests\Cache
 */
class CacheTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Cache
     */
    public $cache;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $storageMock;

    /**
     *
     */
    public function setUp() : void
    {
        $this->storageMock = $this->getMockBuilder(AbstractStorage::class)->getMock();
        $this->cache = new Cache($this->storageMock);
    }

   

    /**
     * Test that CacheStorageInterface::set() is called
     */
    public function testSet()
    {
        $key = 'key';
        $value = 'value';
        $expires = 10;

        $this->storageMock->expects($this->once())
            ->method('set')
            ->with(
                $this->equalTo($key),
                $this->equalTo($value),
                $this->equalTo($expires)
            );

        $this->cache->set($key, $value, $expires);
    }

    /**
     * Test that CacheStorageInterface::set() is called
     */
    public function testSetDateInterval()
    {
        $key = 'key';
        $value = 'value';
        $expires = new \DateInterval('PT1H');      
        $expected = 3600;
        $this->storageMock->expects($this->once())
            ->method('set')
            ->with(
                $this->equalTo($key),
                $this->equalTo($value),
                $this->equalTo($expected)
            );

        $this->cache->set($key, $value, $expires);
    }

    /**
     * Test that CacheStorageInterface::has() invalid key exception
     */
    public function testHasInvalidKey()
    {
        $key = 21021000;
        $this->expectException(InvalidArgumentException::class);
        $this->cache->has($key);
    }


    /**
     * Test that CacheStorageInterface::has() is called and returns true on finding key
     */
    public function testHasFoundWithKey()
    {
        $key = 'key';
        $expected = true;

        $this->storageMock->expects($this->once())
            ->method('has')
            ->with(
                $this->equalTo($key)
            )
            ->willReturn($expected);

        $actual = $this->cache->has($key);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that CacheStorageInterface::has() is called and returns false on failing to find key
     */
    public function testHasFoundNoKey()
    {
        $key = 'key';
        $expected = false;

        $this->storageMock->expects($this->once())
            ->method('has')
            ->with(
                $this->equalTo($key)
            )
            ->willReturn($expected);

        $actual = $this->cache->has($key);
        $this->assertEquals($expected, $actual);
    }


    /**
     * Test that CacheStorageInterface::has() is called and returns false on failing to find key
     */
    public function testGetInvalidKey()
    {
        $key = 21021000;
        $this->expectException(InvalidArgumentException::class);
        $this->cache->get($key);
    }

    /**
     * Test that CacheStorageInterface::get() returns values when key is present.
     */
    public function testGetFoundValue()
    {
        $key = 'key';
        $expected = 'cacheValue';
        $default = 'default value';

        $this->storageMock->expects($this->once())
            ->method('has')
            ->with(
                $this->equalTo($key)
            )
            ->willReturn(true);

        $this->storageMock->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo($key)
            )
            ->willReturn($expected);

        $actual = $this->cache->get($key, $default);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that CacheStorageInterface::get() doesn't return values and default is returned.
     */
    public function testGetDefaultValue()
    {
        $key = 'key';
        $default = 'default value';

        $this->storageMock->expects($this->once())
            ->method('has')
            ->with(
                $this->equalTo($key)
            )
            ->willReturn(false);

        $actual = $this->cache->get($key, $default);
        $this->assertEquals($default, $actual);
    }

    /**
     * Test that CacheStorageInterface::delete() is called
     */
    public function testDelete()
    {
        $key = 'key';
        $this->storageMock->expects($this->once())
            ->method('delete')
            ->with(
                $this->equalTo($key)
            );
        $this->cache->delete($key);
    }

 

    /**
     *
     */
    public function testSetMultiple(){

        $keys = [
            'foo' => 'fooValue',
            'bar' => 'barValue',
            'baz' => 'bazValue'
        ];

        $addStatus = true;
        $expected = true;

        $i = 0;
        $expires = 60 * 60;

        foreach ($keys as $key => $value){
            $this->storageMock->expects($this->at($i))
                ->method('set')
                ->with(
                    $this->equalTo($key),
                    $this->equalTo($value),
                    $this->equalTo($expires)
                )
                ->willReturn($addStatus);
            $i++;
        }

        $actual = $this->cache->setMultiple($keys,$expires);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testSetMultipleWithFailure(){

        $keys = [
            'foo' => 'fooValue',
            'bar' => 'barValue',
            'baz' => 'bazValue'
        ];

        $addStatus = true;
        $expected = false;

        $i = 0;
        $expires = 60 * 60;

        foreach ($keys as $key => $value){

            $status = $addStatus;

            if ($i === 1){
                $status = false;
            }

            $this->storageMock->expects($this->at($i))
                ->method('set')
                ->with(
                    $this->equalTo($key),
                    $this->equalTo($value),
                    $this->equalTo($expires)
                )
                ->willReturn($status);
            $i++;
        }

        $actual = $this->cache->setMultiple($keys,$expires);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testDeleteMultiple(){

        $keys = ['foo','bar','baz'];
        $deleteStatus = true;
        $expected = true;

        foreach ($keys as $i => $key){
            $this->storageMock->expects($this->at($i))
                ->method('delete')
                ->with(
                    $this->equalTo($key)
                )
                ->willReturn($deleteStatus);
        }

        $actual = $this->cache->deleteMultiple($keys);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testDeleteMultipleFailure(){
        $keys = ['foo','bar','baz'];
        $expected = false;
        $deleteStatus = true;
        foreach ($keys as $i => $key){
            if ($i === 1){
                $deleteStatus = false;
            }
            $this->storageMock->expects($this->at($i))
                ->method('delete')
                ->with(
                    $this->equalTo($key)
                )
                ->willReturn($deleteStatus);
        }
        $actual = $this->cache->deleteMultiple($keys);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that storage clear is called.
     */
    public function testclear(){
        $this->storageMock
            ->expects($this->once())
            ->method('clear');

        $this->cache->clear();
    }
}
