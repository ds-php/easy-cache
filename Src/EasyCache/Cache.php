<?php
/**
 * Src/EasyCache/Cache.php
 *
 * @package     Ds\EasyCache
 * @subpackage  Cache
 * @author      Dan Smith <dan--smith@hotmail.co.uk>
 * @version     v.1 (13/04/2018)
 * @copyright   Copyright (c) 2017, Dan Smith
 */
namespace Ds\EasyCache;

use Ds\EasyCache\DateTime\TimeConversion;
use Psr\SimpleCache\CacheInterface as SimpleCache;
use Ds\EasyCache\InvalidArgumentException;
use \DateInterval;

/**
 * PSR 16 Simple Cache Component
 *
 * @package Ds\EasyCache
 */
class Cache extends AbstractCache
{
    /**
     * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
     *
     * @param string        $key    The key of the item to store.
     * @param mixed         $value  The value of the item to store, must be serializable.
     * @param null|int|\DateInterval $ttl   Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     *
     * @throws InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function set($key, $value, $ttl = null)
    {
        return $this->cache->set(
            $key,
            $value,
            TimeConversion::GetTTL($this->cache->ttl, $ttl)
        );
    }


    /**
     * Determines whether an item is present in the cache.
     *
     * NOTE: It is recommended that has() is only to be used for cache warming type purposes
     * and not to be used within your live applications operations for get/set, as this method
     * is subject to a race condition where your has() will return true and immediately after,
     * another script can remove it making the state of your app out of date.
     *
     * @param string $key The cache item key.
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function has($key)
    {
        $this->_isValidKey($key);
        return $this->cache->has($key);
    }

    /**
     * Fetches a value from the cache.
     *
     * @param string $key     The unique key of this item in the cache.
     * @param mixed  $default Default value to return if the key does not exist.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     *
     * @throws InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function get($key, $default = null)
    {
        $this->_isValidKey($key);

        if ($this->cache->has($key)){
            return $this->cache->get($key);
        }

        return $default;
    }

    /**
     * Delete an item from the cache by its unique key.
     *
     * @param string $key The unique cache key of the item to delete.
     * @return bool True if the item was successfully removed. False if there was an error.
     * @throws InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function delete($key)
    {
        $this->_isValidKey($key);
        return $this->cache->delete($key);
    }


    /**
     * Obtains multiple cache items by their unique keys.
     *
     * @param \iterable $keys    A list of keys that can obtained in a single operation.
     * @param mixed    $default Default value to return for keys that do not exist.
     *
     * @return \iterable A list of key => value pairs. Cache keys that do not exist or are stale will have $default as value.
     *
     * @throws InvalidArgumentException
     *   MUST be thrown if $keys is neither an array nor a Traversable,
     *   or if any of the $keys are not a legal value.
     */
    public function getMultiple($keys, $default = null)
    {
        $this->_isTraversable($keys);
        $result = [];

        foreach ((array)$keys as $key){
            $cachedItem = $this->cache->get($key);
            $result[$key] = (null !== $cachedItem) ? $cachedItem : $default;
        }

        return (array)$result;
    }

    /**
     * Persists a set of key => value pairs in the cache, with an optional TTL.
     *
     * @param \iterable              $values A list of key => value pairs for a multiple-set operation.
     * @param DateInterval $ttl    Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     *
     * @throws InvalidArgumentException
     *   MUST be thrown if $values is neither an array nor a Traversable,
     *   or if any of the $values are not a legal value.
     */
    public function setMultiple($values, $ttl = null)
    {
        $this->_hasKeys($values);
        $this->_isTraversable($values);
        $results = [];

        foreach ((array)$values as $key => $value){
            $results[] = $this->cache->set($key, $value, $ttl);
        }

        if ($this->_hasFailure($results)){
            return false;
        }

        return true;
    }

    /**
     * Deletes multiple cache items in a single operation.
     *
     * @param \iterable $keys A list of string-based keys to be deleted.
     *
     * @return bool True if the items were successfully removed. False if there was an error.
     *
     * @throws InvalidArgumentException
     *   MUST be thrown if $keys is neither an array nor a Traversable,
     *   or if any of the $keys are not a legal value.
     */
    public function deleteMultiple($keys)
    {
        $this->_isTraversable($keys);
        $results = [];

        foreach ((array)$keys as $key){
            $results[] = $this->cache->delete($key);
        }

        if ($this->_hasFailure($results)){
            return false;
        }

        return true;
    }

    /**
     * Wipes clean the entire cache's keys.
     *
     * @return bool True on success and false on failure.
     */
    public function clear()
    {
        return $this->cache->clear();
    }

    /**
     * Check that provided key is valid.
     *
     * @param $key
     * @throws InvalidArgumentException
     */
    private function _isValidKey($key){
        if (!is_string($key)){
            throw new InvalidArgumentException('provided key must be a valid string');
        }
    }

    /**
     * Check that $keys are traversable
     *
     * @param $data
     * @throws InvalidArgumentException
     * @return bool
     * @internal
     */
    private function _isTraversable($data){

        if (is_array($data) || $data instanceof \Traversable){
            return true;
        }

        throw new InvalidArgumentException('Keys must be traversable');
    }

    /**
     * @param array $arr
     * @return bool
     * @throws InvalidArgumentException
     */
    private function _hasKeys(array $arr)
    {
        if (array() === $arr || array_keys($arr) === range(0, count($arr) - 1)){
            throw new InvalidArgumentException('Keys missing');
        }

        return true;
    }

    /**
     * Check for failures when adding multiple entries.
     *
     * @param array $results
     * @return bool
     * @internal
     */
    private function _hasFailure(array $results){
        if (in_array(false, $results)){
            return true;
        }
        return false;
    }
}
