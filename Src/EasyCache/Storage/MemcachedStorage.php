<?php
/**
 * Src/EasyCache/Storage/MemcacheStorage.php
 *
 * @package     Ds\EasyCache\Storage
 * @subpackage  Cache
 * @author      Dan Smith <dan--smith@hotmail.co.uk>
 * @version     v.1 (13/04/2018)
 * @copyright   Copyright (c) 2017, Dan Smith
 */
namespace Ds\EasyCache\Storage;

use Ds\EasyCache\DateTime\TimeConversion;

/**
 * Class MemcacheStorage
 *
 * @package Ds\EasyCache\Storage
 */
class MemcacheStorage extends AbstractStorage
{
    /**
     * @var \Memcached
     */
    private $mmc;

    /**
     * MemcacheStorage constructor.
     * @param \Memcached $memcache
     * @param string $server
     * @param int $port
     * @param \DateInterval $ttl
     */
    public function __construct(
        \Memcached $memcache,
        $server = 'localhost',
        $port = 11211,
        \DateInterval $ttl
    )
    {
        $this->mmc = $memcache;
        $this->mmc->addServer($server, $port);
        parent::__construct($ttl);
    }

    /**
     * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
     *
     * @param string   $key   The key of the item to store.
     * @param mixed    $value The value of the item to store, must be serializable.
     * @param null|int|\DateInterval $ttl   Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     */
    public function set($key, $value, $ttl = null)
    {
        $expires = TimeConversion::getTimeStamp($this->ttl,$ttl);
        return $this->mmc->set($key, $value, $expires);
    }

    /**
     * Determines whether an item is present in the cache.
     *
     * NOTE: It is recommended that has() is only to be used for cache warming type purposes
     * and not to be used within your live applications operations for get/set, as this method
     * is subject to a race condition where your has() will return true and immediately after,
     * another script can remove it making the state of your app out of date.
     *
     * @param string $key The cache item key.
     *
     * @return bool
     */
    public function has($key){
        if (!empty($this->mmc->get($key))){
            return true;
        }
        return false;
    }

    /**
     * Fetches a value from the cache.
     *
     * @param string $key     The unique key of this item in the cache.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     *
     */
    public function get($key)
    {
        return $this->mmc->get($key);
    }

    /**
     * Delete an item from the cache by its unique key.
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     */
    public function delete($key)
    {
        return $this->mmc->delete($key);
    }

    /**
     * @param int $delay
     * @return bool
     */
    public function flush($delay = 0)
    {
        return $this->mmc->flush($delay);
    }

    /**
     * Wipes clean the entire cache's keys.
     *
     * @return bool True on success and false on failure.
     */
    public function clear(){
        return $this->flush();
    }

    /**
     * Add new server to Memcache pool.
     * @param $server
     * @param $port
     */
    public function addServer($server, $port)
    {
        $this->mmc->addServer($server, $port);
    }
}
