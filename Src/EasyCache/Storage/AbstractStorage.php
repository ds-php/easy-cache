<?php
/**
 * Src/EasyCache/Storage/AbstractStorage.php
 *
 * @package     Ds\EasyCache\Storage
 * @subpackage  Cache
 * @author      Dan Smith <dan--smith@hotmail.co.uk>
 * @version     v.1 (13/04/2018)
 * @copyright   Copyright (c) 2017, Dan Smith
 */
namespace Ds\EasyCache\Storage;

use DateInterval;
use DateTime;
use Ds\EasyCache\CacheStorageInterface;
use \Psr\SimpleCache\InvalidArgumentException;

/**
 * Class MemcacheStorage
 *
 * @package Ds\EasyCache\Storage
 */
abstract class AbstractStorage implements CacheStorageInterface
{
    /**
     * @var int
     */
    public $ttl;

    /**
     * Abstract Storage constructor.
     * @param \DateInterval|int|null $ttl
     */
    public function __construct(DateInterval $ttl = null)
    {
        if ($ttl === null){
            $this->ttl = 0;
            return;
        }

        $this->ttl = $this->getTtlFromDateInterval($ttl);
    }

    /**
     * With default ttl.
     *
     * @param \DateInterval $ttl
     * @return CacheStorageInterface
     */
    public function withTtl(\DateInterval $ttl){
        $new = clone $this;
        $new->ttl = $this->getTtlFromDateInterval($ttl);
        return $new;
    }

    /**
     * @param \DateInterval $ttl
     * @return int
     */
    protected function getTtlFromDateInterval(\DateInterval $ttl){
        $dateTime = new DateTime();
        $dtnow = $dateTime->getTimestamp();
        $dateTime->add( $ttl );
        return (int)$dateTime->getTimestamp() - $dtnow;
    }


    /**
     * Get ttl as integer.
     *
     * @return \DateInterval
     */
    public function getTtl(){
        return $this->ttl;
    }

    /**
     * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
     *
     * @param string   $key   The key of the item to store.
     * @param mixed    $value The value of the item to store, must be serializable.
     * @param null|int|\DateInterval $ttl   Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     */
    abstract public function set($key, $value, $ttl = null);

    /**
     * Determines whether an item is present in the cache.
     *
     * NOTE: It is recommended that has() is only to be used for cache warming type purposes
     * and not to be used within your live applications operations for get/set, as this method
     * is subject to a race condition where your has() will return true and immediately after,
     * another script can remove it making the state of your app out of date.
     *
     * @param string $key The cache item key.
     *
     * @return bool
     */
    abstract public function has($key);

    /**
     * Fetches a value from the cache.
     *
     * @param string $key     The unique key of this item in the cache.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     *
     */
    abstract public function get($key);

    /**
     * Delete an item from the cache by its unique key.
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     */
    abstract public function delete($key);

    /**
     * Wipes clean the entire cache's keys.
     *
     * @return bool True on success and false on failure.
     */
    abstract public function clear();

}
