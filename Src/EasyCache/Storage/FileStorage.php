<?php
/**
 * Src/EasyCache/Storage/FileStorage.php
 *
 * @package     Ds\EasyCache\Storage
 * @subpackage  Cache
 * @author      Dan Smith <dan--smith@hotmail.co.uk>
 * @version     v.1 (13/04/2018)
 * @copyright   Copyright (c) 2017, Dan Smith
 */
namespace Ds\EasyCache\Storage;

use \DateInterval;
use Ds\EasyCache\CacheException;
use Ds\EasyCache\DateTime\TimeConversion;
use Ds\EasyCache\InvalidArgumentException;

/**
 * Class NullStorage
 *
 * @package Ds\EasyCache
 */
class FileStorage extends AbstractStorage
{
    /**
     * @var string
     */
    const EXTENSION = 'cache';

    /**
     * @var string
     */
    private $dir;

    /**
     * @var bool|array
     */
    private $previousRequest;

    /**
     * File Storage constructor.
     *
     * @param string $directory
     * @param DateInterval $ttl
     * @throws \Exception
     */
    public function __construct($directory, DateInterval $ttl)
    {
        if (!is_string($directory)){
            throw new InvalidArgumentException('Directory must be a valid string');
        }

        $this->dir = $directory;

        if(!@mkdir($this->dir, 0755) && !is_dir($this->dir)){
            throw new CacheException('unable to create directory');
        }

        parent::__construct($ttl);
    }

    /**
     * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
     *
     * @param \string   $key   The key of the item to store.
     * @param mixed    $value The value of the item to store, must be serializable.
     * @param null|int|\DateInterval $ttl   Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     * @throws InvalidArgumentException
     */
    public function set($key, $value, $ttl = null)
    {

        $expires = TimeConversion::GetTTL($this->ttl, $ttl);
        
        $dateTime = new \DateTime();
        $ttltime = $dateTime->getTimestamp() + $expires;

        $cacheItem = $this->_createCacheItem($key, $value, $ttltime);

        if (null !== $value){
            \file_put_contents($cacheItem['file'],$cacheItem['data']);
            if (!file_exists($cacheItem['file'])){
                return false;
            }
        }

        return true;
    }


    /**
     * Determines whether an item is present in the cache.
     *
     * NOTE: It is recommended that has() is only to be used for cache warming type purposes
     * and not to be used within your live applications operations for get/set, as this method
     * is subject to a race condition where your has() will return true and immediately after,
     * another script can remove it making the state of your app out of date.
     *
     * @param string $key The cache item key.
     *
     * @return bool
     */
    public function has($key){
       if ($this->_fetchCacheFile($key)){
         return true;
       }
       return false;
    }

    /**
     * Fetches a value from the cache.
     *
     * @param string $key     The unique key of this item in the cache.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     *
     */
    public function get($key){
        if ($this->has($key)){
            return $this->previousRequest[1];
        }
        return null;
    }

    /**
     * Delete an item from the cache by its unique key.
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     */
    public function delete($key)
    {
        return true;
    }

    /**
     * Wipes clean the entire cache's keys.
     *
     * @return bool True on success and false on failure.
     */
    public function clear(){
        if (is_dir( $this->dir )){
            array_map('unlink', glob("$this->dir/*.*"));
        }

        $data = glob("$this->dir/*.*");

        if (!empty($data)){
            return false;
        }

        return true;
    }

    /**
     * Cache is only called one for both has/get via this internal method.
     *
     * @param $key
     * @return bool|mixed
     * @internal
     */
    private function _fetchCacheFile($key){

        $cacheFile = implode('.',[$key, FileStorage::EXTENSION]);
        $cacheLocation = $this->dir . DIRECTORY_SEPARATOR . $cacheFile;
        $this->previousRequest = false;

        if (file_exists($cacheLocation)){

            $data = \file_get_contents($cacheLocation);
            $cacheData = json_decode($data, TRUE);
            $this->previousRequest = $cacheData;

            //cache has expired
            if ( $cacheData[0] < time() ){
                \unlink($cacheLocation);
                $this->previousRequest = false;
            }
        }
        return $this->previousRequest;
    }

    /**
     * Create cache item.
     *
     * @param string $key
     * @param $value
     * @param int $ttl
     * @return array
     * @throws InvalidArgumentException
     */
    private function _createCacheItem($key, $value, $ttl){

        if (preg_match('/[\\/\\*\\\\\?.]+/i', $key)){
            throw new InvalidArgumentException('$key must be a valid string');
        }

        $filename = implode('.',[(string)$key, FileStorage::EXTENSION]);

        return [
            'file' => $this->dir . DIRECTORY_SEPARATOR . $filename,
            'data' => json_encode([$ttl, $value])
        ];
    }
}
