<?php
/**
 * Src/EasyCache/CacheInterface.php
 *
 * @package     Ds\EasyCache
 * @subpackage  Cache
 * @author      Dan Smith <dan--smith@hotmail.co.uk>
 * @version     v.1 (13/04/2018)
 * @copyright   Copyright (c) 2017, Dan Smith
 */
namespace Ds\EasyCache;

/**
 * Interface CacheInterface
 * @package Ds\EasyCache
 */
interface CacheInterface extends \Psr\SimpleCache\CacheInterface
{
    /**
     * Create new instance with $CacheStorage
     * @param CacheStorageInterface $cacheStorage
     * @return CacheInterface
     */
    public function withCacheStorage(CacheStorageInterface $cacheStorage);

    /**
     * Get Cache Storage
     * @return CacheStorageInterface
     */
    public function getCacheStorage();
}

