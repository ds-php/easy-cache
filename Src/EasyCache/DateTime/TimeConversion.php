<?php

/*
* This file is part the PHP-EasyCache project.
*
* (c) Dan Smith <dan@red-square.co.uk
*
* This source file is subject to the ${LICENCE} license that is bundled
* with this source code in the file LICENSE.
*/

namespace Ds\EasyCache\DateTime;

use \Psr\SimpleCache\InvalidArgumentException;

class TimeConversion
{
    /**
     * @param int $default ttl expires in seconds 
     * @param \DateInterval $ttl   Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     * @return int
     * @throws InvalidArgumentException
     */
    public static function getTimeStamp($default, $ttl = null){

        if ($ttl === null){
            return $default->getTimeStamp();
        }

        if ($ttl instanceof \DateInterval){
            $dateTime = new \DateTime();
            $now = $dateTime->getTimeStamp();
            $dateTime->add( $ttl );
            return $dateTime->getTimestamp() - $now;
        }

        if(strtotime(date('d-m-Y H:i:s', $ttl)) === (int)$ttl) {
            return $ttl;
        }

        throw new InvalidArgumentException('$ttl must be an instance of DateInterval');

    }

    /**
     * @param int $default ttl expires in seconds 
     * @param null|int|\DateInterval $ttl   Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     * @return int
     * @throws InvalidArgumentException
     */
    public static function GetTTL($default, $ttl = null){

        if ($ttl === null){
            return $default;
        }

        if ($ttl instanceof \DateInterval){
            return self::GetTTLfromDateInterval($ttl);
        }

        if(strtotime(date('d-m-Y H:i:s', $ttl)) === (int)$ttl) {
            return $ttl;
        }    

        throw new InvalidArgumentException('$ttl must be an null, int or an instance of DateInterval');
    }

    public static function GetDateInterval($default, $ttl = null){

        if ($ttl === null){
            return $default;
        }

        if ($ttl instanceof \DateInterval){
            return self::GetTTLfromDateInterval($ttl);
        }

        if(strtotime(date('d-m-Y H:i:s', $ttl)) === (int)$ttl) {
            return $ttl;
        }    

        throw new InvalidArgumentException('$ttl must be an null, int or an instance of DateInterval');
    }

    /**
     * @param \DateInterval $di 
     * @return int
     * @throws InvalidArgumentException
     */
    public static function GetTTLfromDateInterval($di){

        if ($di === null){
            return $default->getTimeStamp();
        }

        if ( !($di instanceof \DateInterval) ){
            throw new InvalidArgumentException('$di must be an instance of DateInterval');
        }

        $dateTime = new \DateTime();
        $now = $dateTime->getTimeStamp();
        $dateTime->add( $di );
        return $dateTime->getTimestamp() - $now;    
    }
}