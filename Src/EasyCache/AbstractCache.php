<?php
/**
 * Src/EasyCache/AbstractCache.php
 *
 * @package     Ds\EasyCache
 * @subpackage  Cache
 * @author      Dan Smith <dan--smith@hotmail.co.uk>
 * @version     v.1 (13/04/2018)
 * @copyright   Copyright (c) 2017, Dan Smith
 */
namespace Ds\EasyCache;

use Ds\EasyCache\Storage\NullStorage;

/**
 * Abstract Cache Component
 *
 * @package Ds\EasyCache
 */
abstract class AbstractCache implements CacheInterface
{

    /**
     * @var CacheStorageInterface Cache Storage
     */
    protected $cache;

    /**
     * Creates a new instance of Cache with CacheStorageInterface.
     *
     * @param CacheStorageInterface|null $cacheStorage
     */
    public function __construct(CacheStorageInterface $cacheStorage = null)
    {
        $this->cache = $cacheStorage;

        if ($cacheStorage === null) {
            $this->cache = new NullStorage();
        }
    }

    /**
     * Create new instance with $CacheStorage
     *
     * @param  CacheStorageInterface $cacheStorage
     * @return CacheInterface
     */
    public function withCacheStorage(CacheStorageInterface $cacheStorage)
    {
        $new = clone $this;
        $new->cache = $cacheStorage;
        return $new;
    }

    /**
     * Get Cache Storage
     *
     * @return CacheStorageInterface
     */
    public function getCacheStorage()
    {
        return $this->cache;
    }
}
