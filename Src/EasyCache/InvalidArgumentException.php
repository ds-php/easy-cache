<?php
/**
 * Src/EasyCache/InvalidArgumentException.php
 *
 * @package     Ds\EasyCache
 * @subpackage  Cache
 * @author      Dan Smith <dan--smith@hotmail.co.uk>
 * @version     v.1 (13/04/2018)
 * @copyright   Copyright (c) 2017, Dan Smith
 */
namespace Ds\EasyCache;

use Psr\SimpleCache\InvalidArgumentException as SimpleCacheInvalidArgumentException;

/**
 * Class InvalidArgumentException
 * @package Ds\EasyCache
 */
class InvalidArgumentException extends \Exception implements SimpleCacheInvalidArgumentException
{

}
