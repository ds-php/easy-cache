<?php
/**
 * Src/EasyCache/CacheException.php
 *
 * @package     Ds\EasyCache
 * @subpackage  Cache
 * @author      Dan Smith <dan--smith@hotmail.co.uk>
 * @version     v.1 (13/04/2018)
 * @copyright   Copyright (c) 2017, Dan Smith
 */
namespace Ds\EasyCache;

use Exception;
use Psr\SimpleCache\CacheException as SimpleCacheException;

/**
 * Class CacheException
 * @package Ds\EasyCache
 */
class CacheException extends Exception implements SimpleCacheException
{
}
