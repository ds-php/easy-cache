# PHP Easy Cache - PSR 16 Simple Cache Implementation

[psr/simple-cache](https://github.com/php-fig/simple-cache) For more information on PSR-16.

## Installation

Use Composer to install the component:

`$ composer require ds/easyCache "^v1.0.0"`

## Usage

#### Saving to Cache

```
$cache->set(string $key, mixed $value, DateInterval $expires)
```

* `$key`    - string
Cache keys must be a valid string.

* `$value`  - mixed
Data to be stored in cache

* `$expires` - DateInterval|int|null
If null then default expires is used.

```
$cache->setMultiple($values, DateInterval $expires)
```

* `$values`    - traversable
values must be provided in key => data format, and must be traversable

#### Fetching from Cache

```
$cache->get(string $key, mixed $default = null)
```

* `$key`    - string
Cache keys must be a valid string.

* `$default`    - mixed
Default value to returned in the event that key doesn't exist.


```
$cache->getMultiple($values)
```

* `$values`    - traversable
values must be provided in key => data format, and must be traversable

#### Checking items in cache Storage

```
$cache->has(string $key, mixed $default = null)
```

* `$default`    - mixed
Default value to returned in the event that key doesn't exist.

#### Removing items from cache Storage

```
$cache->delete(string $key)
```

```
$cache->deleteMultiple(traversable $keys)
```

* `$keys`    - array/traversable
List of keys to be removed.

#### Clearing cache Storage

```
$cache->clear()
```

### Storage Adaptors

The following adaptors are available:

* `Rs\Storage\NullStorage`
* `Rs\Storage\FileStorage`
* `Rs\Storage\MemcacheStorage`
* `Rs\Storage\ApcStorage` (no longer supported)

### Creating new Adaptors

* Adaptors must implement `Ds\Cache\CacheStorageInterface`
* Adaptors may extend `Ds\Cache\Storage\AbstractStorage`



### Initialise the component:

```
$cache = new \Ds\Cache\Cache();

```

NullStorage Adaptor is used by default, the above is the same as:

```
$cache = new \Ds\Cache\Cache(
    new \Ds\Cache\NullStoage()
);
```

### Change the `CacheStorage` adaptor:

Update the cache adaptor with `Cache::withCacheStorage`

Method is immutable and returns a new instance of `Ds\Cache`

#### Memcached

```
Ds\Cache\Storage\MemcacheStorage(
    Memcached $memcache,
    string $server,
    int $port,
    DateInterval $ttl
)
```

```
$memcache = $cache->withCacheStorage(
    new \Ds\Cache\Storage\MemcacheStorage(
        new \Memcached(),
        'localhost',
        11211,
        new \DateInterval('P1M')
    )
)
```

#### File Storage

```
Ds\Cache\Storage\FileStorage(
    string $directory,
    DateInterval $ttl
)
```


```
$filestorage = $cache->withCacheStorage(
    new \Ds\Cache\Storage\FileStorage(
        __DIR__ . '/cacheDirectory',
        new \DateInterval('P1M')
    )
)
```

### Tests

Use phpunit To execute the test suite `Tests/Cache`,

```
$ phpunit
```
